package models

/**
 * Created by joseramonrivera on 26/03/14.
 */
case class WebFrameworks(id:Int, name: String, language:String)

object WebFrameworks {
    var webframework = Set(WebFrameworks(1,"Play", "Scala"),
            WebFrameworks(3, "Rails", "Ruby"),
            WebFrameworks(4,"Grails", "Groovy")

    )
  def findAll = webframework.toList.sortBy(_.id)

  def add(id:Int, name: String, language:String) = {
    webframework+=WebFrameworks(id,name,language)
  }
}
